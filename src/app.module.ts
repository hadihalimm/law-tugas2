import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BullModule } from '@nestjs/bull';
import { Md5ReverserModule } from './md5-reverser/md5-reverser.module';

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    Md5ReverserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
