import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { Md5ReverserController } from './md5-reverser.controller';
import { Md5ReverserProcessor } from './md5-reverser.processor';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'estimator',
    }),
  ],
  controllers: [Md5ReverserController],
  providers: [Md5ReverserProcessor],
})
export class Md5ReverserModule {}
