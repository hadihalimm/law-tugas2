import { InjectQueue } from '@nestjs/bull';
import { Body, Controller, Param, Get, Post } from '@nestjs/common';
import { Queue } from 'bull';

@Controller('estimator')
export class Md5ReverserController {
  constructor(@InjectQueue('estimator') private reverserQueue: Queue) {}
  @Post('job')
  async addJob(@Body() body: any) {
    console.log(body);
    const job = await this.reverserQueue.add('estimator', {
      password: body.data,
    });
    return { jobId: job.id };
  }

  @Get('job/:id')
  async getJobState(@Param('id') id: string) {
    const job = await this.reverserQueue.getJob(id);
    const jobState = await job.getState();

    const result = {
      id: id,
      password: job.data.password,
      jobState: jobState,
      response:
        jobState === 'completed'
          ? {
              score: job.returnvalue.score,
              estimatedCrackTime: job.returnvalue.crack_times_display,
              feedback: job.returnvalue.feedback,
            }
          : 'null',
    };
    return result;
  }
}
