import {
  OnGlobalQueueActive,
  OnQueueActive,
  OnQueueCompleted,
  Process,
  Processor,
} from '@nestjs/bull';
import { Job } from 'bull';
import * as zxcvbn from 'zxcvbn';

@Processor('estimator')
export class Md5ReverserProcessor {
  @Process('estimator')
  async reverseHash(job: Job<any>) {
    return await zxcvbn(job.data.password);
  }
}
